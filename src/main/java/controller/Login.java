package controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import service.LoginService;

/**
 * @description 登陆操作控制层
 * @auther 宁晓强
 * @date 2018/12/26 10:47
 */
@Controller
public class Login {

    @Autowired
    private LoginService loginService;

    /**
     * @description 登陆
     * @auther 宁晓强
     * @date 2018/12/26 10:50
     * @param username
     * @param pic
     * @param pwd
     * @param httpSession
     * @return java.lang.String
     */
    @RequestMapping("/loginvalidate")
    public String loginvalidate(@RequestParam("username") String username, @RequestParam("pic") String pic, @RequestParam("password") String pwd, HttpSession httpSession) {
        String picode = (String) httpSession.getAttribute("rand");
        if (!picode.equalsIgnoreCase(pic))
            return "failcode";
        if (username == null)
            return "login";
        String realpwd = loginService.getpwdbyname(username);
        if (realpwd != null && pwd.equals(realpwd)) {
            httpSession.setAttribute("username", username);
            return "index";
        } else
            return "fail";
    }

    /**
     * @description 登录页
     * @auther 宁晓强
     * @date 2018/12/26 10:46
     * @param
     * @return java.lang.String
     */
    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    /**
     * @description 登出 - 跳到登录页
     * @auther 宁晓强
     * @date 2018/12/26 10:47
     * @param httpSession
     * @return java.lang.String
     */
    @RequestMapping("/logout")
    public String logout(HttpSession httpSession) {
        httpSession.removeAttribute("username");
        return "login";
    }
}
