package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @description 验证码
 * @auther 宁晓强
 * @date 2018/12/26 10:54
 */
@Controller
public class Pic {

	/**
	 * @description 图片
	 * @auther 宁晓强
	 * @date 2018/12/26 10:54
	 * @param
	 * @return java.lang.String
	 */
	@RequestMapping(value="/authImg")
	public String getpic(){
		return "authImg";
	}

}
