package po;

import java.util.List;

/**
 * @description 国家信息
 * @auther 宁晓强
 * @date 2018/12/26 10:21
 */
public class Country {

	/**
	 * 国家ID
	 */
	private short country_id;
	/**
	 * 国家
	 */
	private String country;
	/**
	 * 最后更新时间
	 */
	private String last_update;
	/**
	 * 城市信息列表
	 */
	private List<City> citys;

	public short getCountry_id() {
		return country_id;
	}
	public void setCountry_id(short country_id) {
		this.country_id = country_id;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getLast_update() {
		return last_update;
	}
	public void setLast_update(String last_update) {
		this.last_update = last_update;
	}
	public List<City> getCitys() {
		return citys;
	}
	public void setCitys(List<City> citys) {
		this.citys = citys;
	}
	
}
