package po;

/**
 * @description 城市信息
 * @auther 宁晓强
 * @date 2018/12/26 10:18
 */
public class City {

	/**
	 * 城市ID
	 */
	private short city_id;
	/**
	 * 城市
	 */
	private String city;
	/**
	 * 最后更新时间
	 */
	private String last_update;
	/**
	 * 国家信息
	 */
	private Country country;

	public short getCity_id() {
		return city_id;
	}
	public void setCity_id(short city_id) {
		this.city_id = city_id;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Country getCountry() {
		return country;
	}
	public void setCountry(Country country) {
		this.country = country;
	}
	public String getLast_update() {
		return last_update;
	}
	public void setLast_update(String last_update) {
		this.last_update = last_update;
	}

}
