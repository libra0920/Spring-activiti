package service;

import java.util.List;
import java.util.Map;

import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.runtime.ProcessInstance;

import po.LeaveApply;

/**
 * @description 请假申请
 * @auther 宁晓强
 * @date 2018/12/26 15:38
 */
public interface LeaveService {

    /**
     * @description 开启一个流程
     * @auther 宁晓强
     * @date 2019/3/31 11:34
     * @param apply
     * @param userId
     * @return org.activiti.engine.runtime.ProcessInstance
     */
    ProcessInstance startWorkflow(LeaveApply apply, String userId);

    List<LeaveApply> getDeptTaskPage(String userid, int firstrow, int rowcount);

    int getDeptTaskCount(String userId);

    LeaveApply getLeaveApply(int id);

    /**
     * @description 获取HR审批任务列表
     * @auther 宁晓强
     * @date 2019/3/31 13:11
     * @param userId
     * @param firstRow
     * @param rowCount
     * @return java.util.List<po.LeaveApply>
     */
    List<LeaveApply> selectHrTaskPage(String userId, int firstRow, int rowCount);

    int selectHrTaskTotal(String userid);

    List<LeaveApply> getReportBackTaskList(String userId, int firstRow, int rowCount);

    int getReportBackTaskCount(String userid);

    List<LeaveApply> getpageupdateapplytask(String userid, int firstrow, int rowcount);

    int getallupdateapplytask(String userid);

    void completeReportBack(String taskId, String realStartTime, String realEndTime);

    void updatecomplete(String taskid, LeaveApply leave, String reappply);

    List<String> getHighLightedFlows(ProcessDefinitionEntity deployedProcessDefinition, List<HistoricActivityInstance> historicActivityInstances);
}
